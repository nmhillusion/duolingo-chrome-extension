import sass = require("sass");
import fs = require("fs");
import n2mix_ = require("@nmhillusion/n2mix");
import path = require("path");

const parser = n2mix_.parser;

const processArgv = parser.cliParamsParser(process.argv);

const ignores = ["node_modules", "images", ".git"];

const EXTENSIONS_TO_COMPILE = [".packed.js"];

const WORKING_DIR = "./dist";

console.log(" --- RUNNING compileScript ------------ ");
console.log({ processArgv });

function cssContent() {
  return sass.compile(path.join(__dirname, "./scss/app.scss"), {
    style: "dev" === processArgv["mode"] ? "expanded" : "compressed",
  }).css;
}

function getContentOfScirptFile(path_) {
  return fs
    .readFileSync(path_)
    .toString()
    .replace(
      `Object.defineProperty(exports, "__esModule", { value: true });`,
      ""
    );
}

function compileFolder(pathInp) {
  console.log("compileFolder for path: ", pathInp);

  fs.readdirSync(pathInp)
    .filter((path_) => !ignores.includes(path_))
    .forEach((path_) => {
      path_ = `${pathInp}/${path_}`;
      console.log("checkin on path: ", path_);
      const stats = fs.lstatSync(path_);

      if (stats.isDirectory()) {
        compileFolder(`${path_}`);
      } else if (stats.isFile()) {
        for (const extToCompile of EXTENSIONS_TO_COMPILE) {
          console.log("exec on file extension: ", extToCompile);

          if (path_.endsWith(extToCompile)) {
            console.log("\trun for path: ", path_);

            const rawContent = getContentOfScirptFile(path_);
            const out = rawContent.replace("{{ CSS_CONTENT }}", cssContent());

            fs.writeFile(
              path_.replace(extToCompile, ".out.js"),
              out,
              function (err) {
                if (err) {
                  console.error("Error when compile: ", path_, err);
                } else {
                  console.log("compiled: ", path_);
                }
              }
            );
          }
        }
      }
    });
}

compileFolder(WORKING_DIR);
