# How to use Duolingo Chrome Extension

![popup-full](./resource/popup-full.png)

---

> ### Option: `Is Running`

`On`: Extention will be running

`Off`: Extension will be pause until you re-enable it

--

> ### Option: `Home Page Goal`

`On`: Will on ly show panel about daily quest exp.

![Will on ly show panel about daily quest exp](./resource/1.homepage-goal.only-show-exp.enable.png)

`Off`: Default, Duolingo UI

![Default UI](./resource/1.homepage-goal.only-show-exp.disable.png)

--

> ### Option: `Hint of workbank`

`On`: Default, show hint of workbank
![Show hint of workbank (default of Duolingo)](./resource/2.hint-workbank.enable.png)

`Off`: Hidden hint of workbank (even you hover it)
![Hidden hint of workbank (even you hover it)](./resource/2.hint-workbank.disable.png)

--

> ### Option: `AutoClick "Next" buttons`

![AutoClick "Next" buttons](./resource/3.autoclick-next.png)

`On`: Auto-Click `Next` buttons

`Off`: Default, you will click `Next` buttons manually.

--

> ### Option: `Show Correct Status in progress bar`

`On`: Show correct status in progress bar

![Show correct status in progress bar](./resource/4.correct-status.enable.png)

`Off`: Default, not show correct status, default Duolingo UI

![Default, not show correct status, default Duolingo UI](./resource/4.correct-status.disable.png)

--

> ### Option: `Auto enable "Toggle Keyboard Mode"`

![Option: Auto enable "Toggle Keyboard Mode"](./resource/5.auto-enable-toggle-keyboard-mode.png)

`On`: When the screen appears a button to toggle to keyboard mode, extension will press it for you. Note: if the screen has no such a button, extension will do nothing, it only presses whenever Duolingo supports it.

`Off`: Default, press or un-press button Toggle to Keyboard is up to you.


## Keyboard Shortcuts:

> ### Exam Context

`Digit Keyboard`: to select on selectable buttons correspondingly in exam context.

`Esc`: To ignore the current question.

`Ctrl + Space`: To make Duo speaking again current sentence.

> ### Home Context

`Ctrl + Enter`: start entering next lesson.

`Shift + Enter`: start entering practice (to recover hearts).

`Alt + Shift + Q`: start entering story.
