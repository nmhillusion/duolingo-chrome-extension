const fs = require("fs");
const archiver = require("archiver");
const path = require("path");

const output = fs.createWriteStream("package.zip");
const archive = archiver("zip");
const WORKING_DIR = "dist";

function compressForFolder(folderName) {
  console.log("Compress for folder: ", folderName);

  for (const subItem of fs.readdirSync(folderName)) {
    const subItemPath = path.resolve(folderName, subItem);

    if (fs.lstatSync(subItemPath).isFile()) {
      if (
        [".png", ".jpg", ".jpeg", ".lib.js", ".out.js", ".html", ".css"].some(
          (ext) => subItem.endsWith(ext)
        )
      ) {
        console.log("Compress for item in folder: ", subItem, folderName);
        archive.file(folderName + "/" + subItem, fs.readFileSync(subItemPath));
      }
    }
  }
}

output.on("close", function () {
  console.log(archive.pointer() + " total bytes");
  console.log(
    "archiver has been finalized and the output file descriptor has closed."
  );
});

archive.on("error", function (err) {
  throw err;
});

archive.pipe(output);

// archive.directory("images", "/images");
// archive.directory("option", "/option");
// archive.directory("popup", "/popup");
// archive.directory("lib", "/lib");
archive.file("manifest.json", fs.readFileSync("./manifest.json"));

for (const fileItem of fs.readdirSync(path.resolve(WORKING_DIR))) {
  const itemPath = path.resolve(WORKING_DIR, fileItem);
  if (fs.lstatSync(itemPath).isFile()) {
    if (fileItem.includes(".out.")) {
      console.log(
        "Compress for item: ",
        path.join(WORKING_DIR, fileItem),
        " <-- ",
        itemPath
      );
      archive.file(
        path.posix.join(WORKING_DIR, fileItem),
        fs.readFileSync(itemPath)
      );
    }
  }
}

for (const folderName of [
  "images",
  path.posix.join(WORKING_DIR, "option"),
  path.posix.join(WORKING_DIR, "popup"),
  "lib",
]) {
  compressForFolder(folderName);
}

archive.finalize();
