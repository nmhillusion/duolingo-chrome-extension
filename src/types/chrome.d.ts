declare const chrome: {
  runtime: {
    onInstalled: any;
  };
  declarativeContent: {
    onPageChanged: any;
    PageStateMatcher: {
      new ({ pageUrl: { hostContains: string } });
    };
    ShowPageAction: {
      new ();
    };
  };
  storage: {
    sync: {
      set(arg0: { [key: string]: any }, arg1: (error: any) => void): unknown;
      get(eventNames: string[], callback: (...argvs: any[]) => void): void;
    };

    onChanged: {
      addListener(
        listener: (
          changes: {
            [key: string]: {
              newValue: any;
              oldValue: any;
            };
          },
          namespace: any
        ) => void
      ): void;
    };
  };
};
