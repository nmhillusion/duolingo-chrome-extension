export interface DecayedSkillModel {
  name: string | null;
  urlName: any;
  decayed: boolean;
}
