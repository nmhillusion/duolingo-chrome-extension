import { logInfo } from "../module/log";

export function enableKeyboardMode() {
  const btnToggleKeyboard = document.querySelector(
    `[data-test="player-toggle-keyboard"]`
  ) as HTMLButtonElement;

  if (
    btnToggleKeyboard &&
    ["sử dụng bàn phím", "nâng độ khó"].some((it) =>
      String(btnToggleKeyboard.textContent).trim().toLowerCase().includes(it)
    )
  ) {
    logInfo("click to toggle Keyboard mode");
    btnToggleKeyboard.click();
  }
}