import { getSideBarContainer } from "./utility/getSideBarContainer";
import { n2classComp, n2removedClassName } from "./utility/name.constant";
import { StateType } from "./utility/state";

export function removePromotionRank(STATE: StateType) {
  if (STATE.CONFIG.running && STATE.CONFIG.onlyShowExp) {
    const sidebarEl = getSideBarContainer();

    if (sidebarEl) {
      const sidebarChildren = sidebarEl.children;
      for (const child_ of sidebarChildren) {
        if (
          !child_.classList.contains(n2classComp) &&
          !child_.classList.contains(n2removedClassName)
        ) {
          child_.classList.add(n2removedClassName);
        }
      }
    }
  }
}
