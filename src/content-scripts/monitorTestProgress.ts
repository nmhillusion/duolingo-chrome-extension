import {
  getCorrectAnswerEl,
  getIncorrectAnswerEl,
} from "./utility/getAnswerElWithState";
import {
  getProgressBarElement,
  getProgressBarThumbElement,
} from "./utility/getProgressBarElement";
import { n2classComp } from "./utility/name.constant";
import { StateType } from "./utility/state";

const answerStatusClassName = "n2-answer-status";
const alreadyCountedElClass = "n2-already-counted";

export function monitorTestProgress(STATE: StateType) {
  {
    const el_ = getCorrectAnswerEl();
    const checked = el_ && !el_.classList.contains(alreadyCountedElClass);
    if (checked) {
      el_.classList.add(alreadyCountedElClass);
      STATE.STORE.monitor.correctAnswers += 1;
    }
  }
  {
    const el_ = getIncorrectAnswerEl();
    const checked = el_ && !el_.classList.contains(alreadyCountedElClass);
    if (checked) {
      el_.classList.add(alreadyCountedElClass);
      STATE.STORE.monitor.incorrectAnswers += 1;
    }
  }

  // logInfo(" monitorTestProgram = ", STATE.STORE.monitor);
  {
    const progressBarEl = getProgressBarElement();

    if (!progressBarEl) {
      return;
    }

    const progressBarThumbEl = getProgressBarThumbElement(progressBarEl);
    const monitor_ = STATE.STORE.monitor;

    if (
      progressBarThumbEl &&
      0 < monitor_.correctAnswers + monitor_.incorrectAnswers
    ) {
      progressBarEl.style.position = "relative";

      if (!progressBarEl.querySelector("." + answerStatusClassName)) {
        const correctAnswerStatusEl = document.createElement("div");
        correctAnswerStatusEl.classList.add(n2classComp);
        correctAnswerStatusEl.classList.add(answerStatusClassName);

        progressBarEl.appendChild(correctAnswerStatusEl);
      }

      const correctAnswerStatusEl = progressBarEl.querySelector(
        "." + answerStatusClassName
      ) as HTMLDivElement;
      if (correctAnswerStatusEl) {
        updateCorrectAnswerStatusPosition();

        correctAnswerStatusEl.textContent = `${monitor_.correctAnswers}/${
          monitor_.correctAnswers + monitor_.incorrectAnswers
        }`;
      }
    }
  }
}

export function updateCorrectAnswerStatusPosition() {
  const progressBarEl = getProgressBarElement();

  if (!progressBarEl) {
    return;
  }

  const progressBarThumbEl = getProgressBarThumbElement(progressBarEl);

  const correctAnswerStatusEl = progressBarEl.querySelector(
    "." + answerStatusClassName
  ) as HTMLDivElement;
  if (correctAnswerStatusEl) {
    correctAnswerStatusEl.style.left =
      Math.max(
        0,
        progressBarThumbEl.getBoundingClientRect().width -
          correctAnswerStatusEl.getBoundingClientRect().width
      ) + "px";
  }
}
