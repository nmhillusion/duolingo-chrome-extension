import { logInfo } from "../module/log";
import { StateType } from "./utility/state";

const SELECTOR_OF_FILL_INPUT = `[data-test*="challenge"] ._3OOoT`;

export function addStylesheetForDisableHint(STATE: StateType) {
  const CLASS_NAME = "n2-disabled-hint";

  for (const oldCssEl of document.querySelectorAll("." + CLASS_NAME)) {
    oldCssEl.remove();
  }

  logInfo(
    `addStylesheetForDisableHint(showHintOfWordBank: boolean = ${STATE.CONFIG.showHintOfWordBank})`
  );

  if (!STATE.CONFIG.showHintOfWordBank) {
    const cssEl = document.createElement("style");
    cssEl.classList.add(CLASS_NAME);
    cssEl.setAttribute("type", "text/css");
    cssEl.innerHTML = `
      [data-test="hint-token"],
      html[data-duo-theme=dark] [data-test="hint-token"] {
        user-select: none;
        z-index: -1;
        background: none !important;
        background-image: none !important;
      }

      ${SELECTOR_OF_FILL_INPUT} {
        border-bottom: solid 1.3px #36f;
      }
    `.trim();
    logInfo("add stype for disabled show hint of word bank: ", cssEl);

    document.head.appendChild(cssEl);
  }
}
