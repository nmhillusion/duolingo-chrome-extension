import { logError, logInfo } from "../module/log";
import { addElementToBeforeFooterOfSidebar } from "./utility/addElementToBeforeFooterOfSidebar";
import { CONFIG } from "./utility/config";
import { getSideBarContainer } from "./utility/getSideBarContainer";
import { isHomePage } from "./utility/isHomePage";
import { n2classComp } from "./utility/name.constant";
import { getUserId } from "./utility/api/current-user.helper";
import { DailyGoalQuestApi } from "./api/daily-quest.api";
import { checkExistsElementInTreeObject } from "../module/object.helper";
import { formatTimeInSeconds } from "../module/time.helper";

const dailyGoalQuestApi = new DailyGoalQuestApi();

export async function buildGoalOfDailyQuest() {
  if (
    isHomePage() &&
    getSideBarContainer() &&
    !document.querySelector(".n2-goalOfDailyQuestContainer")
  ) {
    logInfo("start buildGoalOfDailyQuest");

    const goalOfDailyQuestContainer = document.createElement("div");
    goalOfDailyQuestContainer.classList.add("n2-goalOfDailyQuestContainer");
    goalOfDailyQuestContainer.classList.add(n2classComp);

    const goalOfDailyQuestEl = document.createElement("div");
    {
      goalOfDailyQuestEl.classList.add("n2-goalOfDailyQuest");
      goalOfDailyQuestEl.classList.add(n2classComp);
    }

    goalOfDailyQuestContainer.appendChild(goalOfDailyQuestEl);
    addElementToBeforeFooterOfSidebar(goalOfDailyQuestContainer);
  }

  {
    // update for daily quest info

    const dailyGoalPanel = document.querySelector(
      ".n2-goalOfDailyQuestContainer .n2-goalOfDailyQuest"
    );

    if (dailyGoalPanel && 0 == dailyGoalPanel.childElementCount) {
      try {
        const goalOfDailyQuest = await dailyGoalQuestApi.exec(getUserId());

        logInfo("goalOfDailyQuest = ", goalOfDailyQuest);

        if (!goalOfDailyQuest) {
          throw new Error("cannot obtain Duo Goal of Daily Quest");
        }

        let dailyGoalXP = 0;
        let dailyQuestSpentTime = 0;

        if (
          checkExistsElementInTreeObject(
            goalOfDailyQuest,
            "goals",
            "progress",
            "daily_goal_daily_quest"
          ) &&
          Number.isInteger(
            Number(
              goalOfDailyQuest["goals"]["progress"]["daily_goal_daily_quest"]
            )
          )
        ) {
          dailyGoalXP = Number(
            goalOfDailyQuest["goals"]["progress"]["daily_goal_daily_quest"]
          );
        }
        if (
          checkExistsElementInTreeObject(
            goalOfDailyQuest,
            "goals",
            "progress",
            "time_spent_core_daily_quest"
          ) &&
          Number.isInteger(
            Number(
              goalOfDailyQuest["goals"]["progress"][
                "time_spent_core_daily_quest"
              ]
            )
          )
        ) {
          dailyQuestSpentTime = Number(
            goalOfDailyQuest["goals"]["progress"]["time_spent_core_daily_quest"]
          );
        }

        {
          const dailyGoalXPEl = document.createElement("div");
          dailyGoalXPEl.classList.add("n2-goalOfDailyQuest-dailyGoalXP");
          dailyGoalXPEl.textContent = `Daily Exp: ${dailyGoalXP}`;

          const dailyQuestSpentTimeEl = document.createElement("div");
          dailyQuestSpentTimeEl.classList.add(
            "n2-goalOfDailyQuest-dailyQuestSpentTime"
          );
          dailyQuestSpentTimeEl.textContent = `Spent time: ${formatTimeInSeconds(
            dailyQuestSpentTime
          )}`;

          if (dailyGoalPanel && 0 == dailyGoalPanel.childElementCount) {
            dailyGoalPanel.appendChild(dailyGoalXPEl);
            dailyGoalPanel.appendChild(dailyQuestSpentTimeEl);
          }
        }
      } catch (err) {
        logError(err);
      }
    }
  }

  CONFIG.ACTIVE_HREF_DAILY_QUEST = location.href;
}
