import { logInfo, logWarn } from "../module/log";
import { getLastSpeakerButton } from "./utility/getLastSpeakerButton";
import { getSelectableButton } from "./utility/getSelectableButton";
import { getSkipButton } from "./utility/getSkipButton";
import { StateType } from "./utility/state";

let hasSetupShortcuts = false;

export function setupShortcuts(STATE: StateType) {
  if (STATE.CONFIG.running && !hasSetupShortcuts) {
    hasSetupShortcuts = true;

    window.onkeydown = null;
    window.onkeypress = null;

    window.removeEventListener("keyup", keyupEventHandler);
    // listen on keyup event due to keydown cannot catch some keyboard events, for example: number keyboard events
    window.addEventListener("keyup", keyupEventHandler, {
      capture: true,
      once: false,
      passive: false,
    });
  }
}

function keyupEventHandler(evt: KeyboardEvent) {
  const pressKey = String(evt.key).trim() || evt.code;
  // logInfo({ pressKey });

  if (Number.isInteger(Number(pressKey))) {
    const selectedNumber = Number(pressKey);

    const selectableButtons = getSelectableButton();
    if (!selectableButtons || 1 > selectableButtons.length) {
      logWarn("not found any button to click");
      return;
    }

    if (selectableButtons) {
      for (const btn_ of selectableButtons) {
        btn_.onkeydown = null;
        btn_.onkeyup = null;
        btn_.onkeypress = null;
      }
    }

    // logInfo({ selectedNumber, selectableButtons });

    if (
      0 <= selectedNumber &&
      selectedNumber <= Math.min(9, selectableButtons.length)
    ) {
      const selectedButtonIdx = 0 == selectedNumber ? 9 : selectedNumber - 1;

      // logInfo(
      //   "will click on ",
      //   selectedNumber,
      //   selectableButtons[selectedButtonIdx]
      // );
      selectableButtons[selectedButtonIdx]?.click();
    }

    return;
  }

  if (evt.shiftKey) {
    if ("l" == String(pressKey).toLowerCase()) {
      // shift + l
      logInfo("shortcut to lesson");
      window.location.href = "/lesson";

      evt.preventDefault();
      evt.stopPropagation();
    } else if ("p" == String(pressKey).toLowerCase()) {
      // shift + p
      logInfo("shortcut to practice");
      window.location.href = "/practice";

      evt.preventDefault();
      evt.stopPropagation();
    }
  }

  // if ("p" == String(pressKey).toLowerCase()) {
  //   if (evt.shiftKey && evt.altKey) {
  //     // alt + shift + p
  //     logInfo("shortcut to practice");
  //     window.location.href = "/practice";
  //   }
  // }

  if ("q" == String(pressKey).toLowerCase()) {
    if (evt.shiftKey && evt.altKey) {
      // alt + shift + q
      logInfo("shortcut to stories");
      window.location.href = "/stories";
    }
  }

  if ("space" == String(pressKey).toLowerCase()) {
    if (evt.ctrlKey) {
      // ctrl + space
      logInfo("shortcut to speak again");
      getLastSpeakerButton()?.click();
    }
  }

  if ("escape" == String(pressKey).toLowerCase()) {
    const skipBtn = getSkipButton();
    skipBtn?.click();
  }

  if ("home" == String(pressKey).toLowerCase()) {
    if (evt.ctrlKey) {
      // Ctrl + Home
      logInfo("shortcut to go home");
      window.location.href = "/";
    }
  }
}
