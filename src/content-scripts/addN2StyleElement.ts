import { MY_CLASS } from "./utility/name.constant";
import { StateType } from "./utility/state";

export function addN2StyleElement(STATE: StateType){
  for (const oldCssEl of document.querySelectorAll("." + MY_CLASS)) {
    oldCssEl.remove();
  }
  if (STATE.CONFIG.running) {
    const styleEl = document.createElement("style");
    styleEl.classList.add(MY_CLASS);

    styleEl.innerHTML = styleContent();

    document.head.appendChild(styleEl);
  }
}

function styleContent() {
  return `
  {{ CSS_CONTENT }}    
  `;
}