/// <reference path="../types/metrix.d.ts" />

import { CONFIG } from "./utility/config";
import { StateType } from "./utility/state";

export function removeTournamentBoard(STATE: StateType) {
  if (STATE.CONFIG.running && STATE.CONFIG.onlyShowExp) {
    const boardEls = _m
      .dom()
      .querySelectorAll({ selector: "div", text: "Giải đấu" });
    if (boardEls && CONFIG.TOURNAMENT_DIV_INDEX < boardEls.length) {
      const targetEl = boardEls[CONFIG.TOURNAMENT_DIV_INDEX];
      if (targetEl) {
        targetEl.remove();
      }
    }
  }
}