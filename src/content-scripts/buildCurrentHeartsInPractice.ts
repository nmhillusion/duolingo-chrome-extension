import { logDebug, logError, logInfo, logWarn } from "../module/log";
import { HealthApi } from "./api/health.api";
import { getUserId } from "./utility/api/current-user.helper";
import { getProgressBarElement } from "./utility/getProgressBarElement";
import { isPracticePage } from "./utility/isPracticePage";
import { StateType } from "./utility/state";

const healthApi = new HealthApi();

export function buildCurrentHeartsInPractice(STATE: StateType) {
  if (!isPracticePage() || !STATE.CONFIG.showExtraStatusInTest) {
    return;
  }

  const progressBarEl = getProgressBarElement();

  if (!progressBarEl) {
    return;
  }

  if (
    !progressBarEl.querySelector(".n2-heart-panel") &&
    -1 == STATE.STORE.monitor.currentHeartsInPractice
  ) {
    healthApi
      .exec(getUserId())
      .then((data) => {
        logInfo("health date: ", data);

        if (data && data.health && 0 < data.health.maxHearts) {
          STATE.STORE.monitor.currentHeartsInPractice = data.health.hearts;

          const heartPanel = document.createElement("div");
          heartPanel.classList.add("n2-heart-panel");

          {
            const heartIcon = document.createElement("span");
            heartIcon.classList.add("n2-heart-icon");
            heartIcon.textContent = "❤️";
            heartPanel.appendChild(heartIcon);
          }

          {
            const heartValue = document.createElement("span");
            heartValue.classList.add("n2-heart-value");
            heartValue.textContent = String(data.health.hearts);
            heartPanel.appendChild(heartValue);
          }

          progressBarEl.appendChild(heartPanel);
        }
      })
      .catch((error) => {
        logError("buildCurrentHeartsInPractice error: ", error);
      });
  }
}
