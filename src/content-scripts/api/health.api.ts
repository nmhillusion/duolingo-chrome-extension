// https://www.duolingo.com/2017-06-30/users/93846474?fields=currentCourse,health,rewardBundles,streakData%7BcurrentStreak,longestStreak,previousStreak%7D,xpGains&_=1719113668991
// https://d35aaqx5ub95lt.cloudfront.net/images/hearts/7631e3ee734dd4fe7792626b59457fa4.svg

import { FetchHelper } from "../utility/api/fetch.helper";
import { ApiBase } from "./api.base";

export interface HealthData {
  health: {
    maxHearts: number;
    eligibleForFreeRefill: boolean;
    healthEnabled: boolean;
    secondsPerHeartSegment: number;
    unlimitedHeartsAvailable: boolean;
    hearts: number;
    secondsUntilNextHeartSegment: number;
    useHealth: boolean;
  };
}

export class HealthApi extends ApiBase<HealthData> {
  constructor() {
    super(15 * 1000);
  }

  override __exec__() {
    const apiLink = `https://www.duolingo.com/2017-06-30/users/${this.userId}`;
    const params = {
      fields: "health",
      timezone: "Asia/Bangkok",
      ui_language: "en",
      _: this.latestUpdate,
    };

    const info = new FetchHelper().getJson(apiLink, params, {
      Accept: "application/json; charset=UTF-8",
    });

    return info;
  }
}
