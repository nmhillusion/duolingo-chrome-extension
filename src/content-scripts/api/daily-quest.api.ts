import { FetchHelper } from "../utility/api/fetch.helper";
import { ApiBase } from "./api.base";

export class DailyGoalQuestApi extends ApiBase<any> {
  constructor() {
    super(1 * 60 * 1000);
  }

  override __exec__() {
    const apiLink = `https://goals-api.duolingo.com/users/${this.userId}/progress`;

    const params: { [key: string]: string | number | boolean } = {
      timezone: "Asia/Bangkok",
      ui_language: "en",
      _: this.latestUpdate,
    };

    const info = new FetchHelper().getJson(apiLink, params, {
      Accept: "application/json; charset=UTF-8",
    });

    return info;
  }
}
