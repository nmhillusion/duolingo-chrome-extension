import { logDebug } from "../../module/log";

export abstract class ApiBase<T> {
  protected userId: string = null;
  private info: T | PromiseLike<T> = null;
  protected latestUpdate: number = null;
  private isExecuting: boolean = false;

  constructor(private EXPIRED_DATA_TIME_IN_MILLIS: number = 1 * 60 * 1000) {}

  private isExpiredData() {
    return (
      new Date().getTime() - this.EXPIRED_DATA_TIME_IN_MILLIS >
      this.latestUpdate
    );
  }

  protected abstract __exec__(): Promise<T>;

  public async exec(userId: string): Promise<T> {
    if (this.isExecuting) {
      return Promise.reject("executing");
    }

    try {
      this.isExecuting = true;

      if (this.info && !this.isExpiredData()) {
        logDebug("existed info: ", this.info);
        return Promise.resolve(this.info);
      }
      this.userId = userId;
      this.latestUpdate = new Date().getTime();
      this.info = await this.__exec__();
      return Promise.resolve(this.info);
    } catch (ex) {
      console.error(ex);
      return Promise.reject(ex);
    } finally {
      this.isExecuting = false;
    }
  }
}
