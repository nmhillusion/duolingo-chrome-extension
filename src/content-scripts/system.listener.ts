import { logInfo } from "../module/log";
import { buildCurrentHeartsInPractice } from "./buildCurrentHeartsInPractice";
import {
  getProgressBarElement,
  getProgressBarThumbElement,
} from "./utility/getProgressBarElement";
import { StateType } from "./utility/state";

const LISTENERS = {
  onPageChanged: {} as {
    [key: string]: (newUrl: string, oldUrl: string) => void;
  },
  onRePractice: {} as {
    [key: string]: () => void;
  },
};

export class SystemListener {
  constructor(private STATE: StateType) {
    logInfo("init for system listener");
  }

  public registerForListeners() {
    LISTENERS.onPageChanged["resetMonitorStatus"] = (newUrl, oldUrl) => {
      this.STATE.STORE.monitor.correctAnswers =
        this.STATE.STORE.monitor.incorrectAnswers = 0;

      this.STATE.STORE.monitor.currentHeartsInPractice = -1;
    };

    LISTENERS.onRePractice["resetMonitorStatus"] = () => {
      this.STATE.STORE.monitor.correctAnswers =
        this.STATE.STORE.monitor.incorrectAnswers = 0;

      this.STATE.STORE.monitor.currentHeartsInPractice = -1;
    };
  }

  public detectChangePageUrl() {
    if (!this.STATE.currentUrl) {
      this.STATE.currentUrl = location.href;
    } else {
      const currentUrl = location.href;

      if (currentUrl != this.STATE.currentUrl) {
        this.emitPageChanged(currentUrl);

        this.STATE.currentUrl = currentUrl;
      }
    }
  }

  private emitPageChanged(newUrl: string) {
    for (const listenerKey_ of Object.keys(LISTENERS.onPageChanged)) {
      logInfo(`exec for listener on event[onPageChanged]: `, listenerKey_);

      LISTENERS.onPageChanged[listenerKey_](newUrl, this.STATE.currentUrl);
    }
  }

  public detectRePractice() {
    const monitor_ = this.STATE.STORE.monitor;
    const countTotalAnswers =
      monitor_.correctAnswers + monitor_.incorrectAnswers;

    if (2 < countTotalAnswers) {
      // at least 2 answers from the last test to check for this reset
      // console.log({ countTotalAnswers });

      const progressBarEl = getProgressBarElement();

      if (progressBarEl) {
        const progressBarThumbEl = getProgressBarThumbElement(progressBarEl);

        if (
          !progressBarThumbEl ||
          0 == progressBarThumbEl.getBoundingClientRect().width
        ) {
          this.emitRePractice();
        }
      }
    }
  }

  private emitRePractice() {
    const rePracticeListers = LISTENERS.onRePractice;

    for (const key_ of Object.keys(rePracticeListers)) {
      logInfo(`exec for listener on event[onRePractice]: `, key_);

      rePracticeListers[key_]();
    }
  }
}
