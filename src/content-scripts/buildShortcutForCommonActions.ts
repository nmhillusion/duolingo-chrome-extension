import { logInfo } from "../module/log";
import { addElementToBeforeFooterOfSidebar } from "./utility/addElementToBeforeFooterOfSidebar";
import { n2classComp } from "./utility/name.constant";

export function buildShortcutForCommonActions() {
  if (!document.querySelector(".n2-shortcutForCommonActions")) {
    const shortcutActionsContainer = document.createElement("div");
    shortcutActionsContainer.classList.add("n2-shortcutForCommonActions");
    shortcutActionsContainer.classList.add(n2classComp);

    {
      const btnLesson = document.createElement("button");
      btnLesson.textContent = "Lesson";
      btnLesson.classList.add("n2-btn");
      btnLesson.classList.add("n2-btn-lesson");
      btnLesson.onclick = (_) => {
        logInfo("do click to lesson");
        window.location.href = "/lesson";
      };

      {
        const shortcutEl = document.createElement("span");
        shortcutEl.classList.add("n2-shortcut-desc");
        shortcutEl.textContent = "(Shift + L)";
        btnLesson.appendChild(shortcutEl);
      }

      shortcutActionsContainer.appendChild(btnLesson);
      addElementToBeforeFooterOfSidebar(shortcutActionsContainer);
    }

    {
      const btnPractice = document.createElement("button");
      btnPractice.textContent = "Practice";
      btnPractice.classList.add("n2-btn");
      btnPractice.classList.add("n2-btn-practice");
      btnPractice.onclick = (_) => {
        logInfo("do click to practice");
        window.location.href = "/practice";
      };

      {
        const shortcutEl = document.createElement("span");
        shortcutEl.classList.add("n2-shortcut-desc");
        shortcutEl.textContent = "(Shift + P)";
        btnPractice.appendChild(shortcutEl);
      }

      shortcutActionsContainer.appendChild(btnPractice);
      addElementToBeforeFooterOfSidebar(shortcutActionsContainer);
    }

    {
      const btnStory = document.createElement("button");
      btnStory.textContent = "Story";
      btnStory.classList.add("n2-btn");
      btnStory.classList.add("n2-btn-story");
      btnStory.onclick = (_) => {
        logInfo("do click to Story");
        window.location.href = "/practice-hub/stories";
      };

      {
        const shortcutEl = document.createElement("span");
        shortcutEl.classList.add("n2-shortcut-desc");
        shortcutEl.textContent = "(Alt + Shift + Q)";
        btnStory.appendChild(shortcutEl);
      }

      shortcutActionsContainer.appendChild(btnStory);
    }
  }
}
