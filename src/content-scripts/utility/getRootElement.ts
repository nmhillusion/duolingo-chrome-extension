export function getRootElement() {
  return document.querySelector("#root") as HTMLElement;
}
