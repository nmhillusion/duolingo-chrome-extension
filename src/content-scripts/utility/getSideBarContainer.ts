export function getSideBarContainer() {
  return document.querySelector(
    `[data-test="home"] > *:nth-child(1) > *:first-child`
  );
}