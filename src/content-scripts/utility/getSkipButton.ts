import { getSessionPlayerFooter } from "./getSessionPlayerFooter";

export function getSkipButton() {
  const sessionPlayerEl = getSessionPlayerFooter();
  if (sessionPlayerEl) {
    const skipBtnList = sessionPlayerEl.querySelectorAll(
      `[data-test="player-skip"]`
    );

    if (1 == skipBtnList.length) {
      return skipBtnList.item(0) as HTMLDivElement;
    } else {
      return null;
    }
  }
}
