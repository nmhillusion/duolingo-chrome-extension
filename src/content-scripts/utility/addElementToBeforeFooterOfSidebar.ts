import { getSideBarContainer } from "./getSideBarContainer";

export function addElementToBeforeFooterOfSidebar(el2add: Element) {
  const sidebarContainer = getSideBarContainer();

  if (sidebarContainer) {
    const footerEl = sidebarContainer.querySelector(
      `[data-test="footer-links"]`
    );
    if (footerEl) {
      sidebarContainer.insertBefore(el2add, footerEl);
    } else {
      sidebarContainer.appendChild(el2add);
    }
  }
}