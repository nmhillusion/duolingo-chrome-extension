import { coolingTask } from "../../module/cooling-task";
import { repeatActionWithDebounce } from "../../module/repeat-debounce";
import { CONFIG } from "./config";
import { fnGetBtnNextList } from "./fnGetBtnNextList";
import {
  getCorrectAnswerEl,
  getIncorrectAnswerEl,
} from "./getAnswerElWithState";
import { getProgressBarElement } from "./getProgressBarElement";
import { getSessionPlayerFooter } from "./getSessionPlayerFooter";
import { isHomePage } from "./isHomePage";
import { StateType } from "./state";

export function findAndClickNextButton(STATE: StateType) {
  if (isHomePage()) {
    // logInfo("return:homepage");
    return;
  }

  if (getSessionPlayerFooter() && getProgressBarElement()) {
    if (!getIncorrectAnswerEl() && !getCorrectAnswerEl()) {
      // logInfo("return:Is answering, not submit");
      // Is answering, not submit
      return;
    }
  }

  if (
    STATE.STORE.cooling.coolingClickNextButton &&
    STATE.STORE.cooling.coolingClickNextButton.isCooling()
  ) {
    return;
  }
  STATE.STORE.cooling.coolingClickNextButton = coolingTask(
    CONFIG.COOLING.COOLING_TIME_IN_MILLIS
  );

  const incorrectElement = !!getIncorrectAnswerEl();

  const nextButtons = fnGetBtnNextList();
  // logInfo("return:", { nextButtons, incorrectElement });
  if (nextButtons && 0 < nextButtons.length && !incorrectElement) {
    // logInfo(">> start click next button: ", nextButtons);

    nextButtons.forEach((btn_) => {
      repeatActionWithDebounce(
        () => {
          // logInfo("click on next button: ", btn_);
          btn_?.click(); // COMMENT FOR TESTING

          return (
            !!btn_ &&
            !btn_.disabled &&
            "true" !== btn_.getAttribute("aria-disabled")
          );
        },
        3,
        1_000
      );
    });

    // logInfo("<< end click next button.");
  }
}
