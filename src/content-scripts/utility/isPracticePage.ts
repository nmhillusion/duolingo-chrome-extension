import { CONFIG } from "./config";

export function isPracticePage() {
  const path_ = location.pathname;

  return !path_ || path_ === CONFIG.PRACTICE_PAGE_URL;
}