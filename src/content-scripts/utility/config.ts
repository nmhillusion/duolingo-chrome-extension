export const CONFIG = {
  TOURNAMENT_DIV_INDEX: 7,
  INTERVAL_IN_MILLIS: 600,
  ACTIVE_HREF_DAILY_QUEST: null,
  HOME_PAGE_URL: "/learn",
  PRACTICE_PAGE_URL: "/practice",  
  // BROWSER_SETTINGS: {
  //   coachEnabled: false,
  //   completeReverseTranslationTypingEnabled: true,
  //   prefersReducedMotion: true,
  //   typingEnabled: true,
  // },
  COOLING: {
    COOLING_TIME_IN_MILLIS: 1000
  },
};