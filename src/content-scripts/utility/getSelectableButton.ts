import { findButtonInTreeNode } from "../../module/findButtonInTreeNode";
import { getProgressBarElement } from "./getProgressBarElement";
import { getSessionPlayerFooter } from "./getSessionPlayerFooter";

export function getSelectableButton() {
  let elements: NodeListOf<HTMLButtonElement> | HTMLButtonElement[] =
    document.querySelectorAll(`ul li button`);

  if (0 == elements.length) {
    elements = document.querySelectorAll(
      `button[data-test='challenge-tap-token']`
    );
  }

  if (0 == elements.length) {
    elements = getWordBankSelectableButtons();
  }

  if (0 == elements.length) {
    const elements_ = document.querySelectorAll(
      `[data-test="stories-element"] button [data-test="challenge-tap-token-text"]`
    );

    if (0 < elements_.length) {
      elements = [];
      for (const el_ of elements_) {
        const excludedSelector = "._2R_o5._2S0Zh._2TrnF";
        const btnEl = findButtonInTreeNode(el_, excludedSelector);
        if (btnEl) {
          elements.push(btnEl);
        }
      }
    }
  }

  if (0 == elements.length) {
    const elements_ = document.querySelectorAll(`button ._16XFG`);

    if (0 < elements_.length) {
      elements = [];
      for (const el_ of elements_) {
        const btnEl = findButtonInTreeNode(el_);
        if (btnEl) {
          elements.push(btnEl);
        }
      }
    }
  }

  if (0 == elements.length) {
    if (!getProgressBarElement()) {
      const footerEl = getSessionPlayerFooter();

      if (footerEl) {
        const elements_ = footerEl.querySelectorAll("button");

        if (0 < elements_.length) {
          elements = [];
          for (const el_ of elements_) {
            const btnEl = findButtonInTreeNode(el_);
            if (btnEl) {
              elements.push(btnEl);
            }
          }
        }
      }
    }
  }

  // logInfo("getSelectableButton() -> ", elements);

  return elements;
}

function getWordBankSelectableButtons() {
  const elements: HTMLButtonElement[] = [];
  const elements_ = document.querySelectorAll(
    `
    [data-test*="challenge"] [data-test='word-bank'] button ._27oGP,
    [data-test*="challenge-tapComplete"] [data-test='word-bank'] button [data-test='challenge-tap-token-text'],
    [data-test*="challenge-patternTapComplete"] [data-test='word-bank'] button [data-test='challenge-tap-token-text'],
    [data-test*="stories-element"] [data-test='word-bank'] button
    `.trim()
  );

  for (const el_ of elements_) {
    const btnEl = findButtonInTreeNode(el_);
    if (btnEl) {
      elements.push(btnEl);
    }
  }

  return elements;
}
