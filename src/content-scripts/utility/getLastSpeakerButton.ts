import { getRootElement } from "./getRootElement";

export function getLastSpeakerButton(): HTMLElement | null {
  const root = getRootElement();
  if (!root) {
    return null;
  }

  const speakerButtons = Array.from(
    root.querySelectorAll(`[data-test="audio-button"]`)
  ).filter((it) => {
    const rect = it.getBoundingClientRect();
    return 0 < rect.width * rect.height;
  });

  if (speakerButtons.length > 0) {
    return speakerButtons[speakerButtons.length - 1] as HTMLElement;
  }

  return null;
}
