import { CoolingState } from "../../module/cooling-task";
import { ElementObservation } from "../../module/element-observation";

export interface StateType {
  LISTENER: {
    EL_OBSERVABLE_LISTENER: {
      instance: ElementObservation;
      isRunning: boolean;
      startLoopFunc(): void;
      endLoopFunc(): void;
    };
  };
  TIMER: {
    intervalTicker: {
      isRunning: boolean;
      instance: any;
      latestTickTimestamp: Date;
      startLoopFunc(): void;
      endLoopFunc(): void;
    };
  };
  currentUrl: string;
  CONFIG: {
    running: boolean;
    onlyShowExp: boolean;
    showHintOfWordBank: boolean;
    autoClickNext: boolean;
    showExtraStatusInTest: boolean;
    autoEnableToggleToKeyboard: boolean;
  };
  STORE: {
    monitor: {
      correctAnswers: number;
      incorrectAnswers: number;
      currentHeartsInPractice: number;
    };
    cooling: {
      coolingMonitorNextButton: CoolingState;
      coolingClickNextButton: CoolingState;
    };
  };
}
