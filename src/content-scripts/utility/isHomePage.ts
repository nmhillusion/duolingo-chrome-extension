import { CONFIG } from "./config";

export function isHomePage() {
  const path_ = location.pathname;

  return !path_ || path_ === CONFIG.HOME_PAGE_URL;
}