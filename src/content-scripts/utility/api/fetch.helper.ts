import { logInfo } from "../../../module/log";
import { parseCookie } from "./cookie.helper";
import { ParamValueType } from "./param.type";
import { combineQueryParams } from "./query-param.helper";

export class FetchHelper {
  private COOKIE_NAME__JWT_TOKEN = "jwt_token";

  ///////////////////////////////////////////////

  private fillAuthorization(headers: { [key: string]: string } = {}) {
    const cookies = parseCookie();
    // logInfo("cookies: ", cookies);

    const jwtToken = cookies[this.COOKIE_NAME__JWT_TOKEN];
    if (jwtToken) {
      headers["Authorization"] = `Bearer ${jwtToken}`;
    }
  }

  public async getJson(
    url_: string,
    params: { [key: string]: ParamValueType } = {},
    headers: { [key: string]: string } = {}
  ) {
    const combinedUrl_ = combinedUrl(url_, params);
    logInfo("GET ", combinedUrl_);
    this.fillAuthorization(headers);

    return fetch(combinedUrl_, { headers }).then((res) => res.json());
  }
}

function combinedUrl(url_: string, params: { [key: string]: ParamValueType }): string {
  return url_ + (url_.includes("?") ? "&" : "?") + combineQueryParams(params);
}
