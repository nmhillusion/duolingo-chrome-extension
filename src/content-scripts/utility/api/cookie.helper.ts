export function parseCookie() {
  const result: { [key: string]: string } = {};

  const cookie_ = document.cookie;
  if (cookie_) {
    cookie_.split(";").forEach((cookie) => {
      const [key, value] = cookie.trim().split("=");
      result[String(key).trim()] = String(value).trim();
    });
  }

  return result;
}
