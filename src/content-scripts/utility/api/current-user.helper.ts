
export function getUserId() {
  const avatarsIntro = JSON.parse(localStorage.getItem("duo.avatarsIntro"));

  if (avatarsIntro) {
    return Object.keys(avatarsIntro)[0];
  } else {
    return null;
  }
}