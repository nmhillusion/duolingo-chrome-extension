import { ParamValueType } from "./param.type";

export function combineQueryParams(params: { [key: string]: ParamValueType }) {
  return Object.keys(params)
    .map((key) => `${key}=${params[key]}`)
    .join("&");
}
