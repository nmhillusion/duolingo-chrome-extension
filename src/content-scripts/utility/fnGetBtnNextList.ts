import { findButtonInTreeNode } from "../../module/findButtonInTreeNode";
import { logDebug } from "../../module/log";
import {
  getCorrectAnswerEl,
  getIncorrectAnswerEl,
} from "./getAnswerElWithState";
import { getProgressBarElement } from "./getProgressBarElement";
import { getSessionPlayerFooter } from "./getSessionPlayerFooter";

export function fnGetBtnNextList() {
  let buttonPreList = [];

  if (
    getProgressBarElement() &&
    (getCorrectAnswerEl() || getIncorrectAnswerEl())
  ) {
    buttonPreList = buttonPreList.concat(
      Array.from(
        document.querySelectorAll(
          `button[data-test="player-next"]:not([aria-disabled="true"]),
          [data-test="stories-player-done"]:not([disabled])
      `.trim()
        )
      )
    );
  }

  const footerEl = getSessionPlayerFooter();
  if (footerEl) {
    buttonPreList = buttonPreList.concat(
      Array.from(
        footerEl.querySelectorAll(
          `button._1N-oo:not([data-test="player-skip"])`
        )
      )
    );
  }

  if (getProgressBarElement()) {
    buttonPreList = buttonPreList.concat(
      Array.from(
        document.querySelectorAll(
          `
          button[data-test="stories-player-continue"]:not([disabled])
      `.trim()
        )
      )
    );
  }

  if (0 == buttonPreList.length) {
    const footerEl = getSessionPlayerFooter();
    if (getProgressBarElement() && footerEl) {
      const allBtnInFooter = Array.from(footerEl.querySelectorAll(`button`));

      if (1 == allBtnInFooter.length) {
        buttonPreList.push(allBtnInFooter[0]);
      }

      // logDebug({ allBtnInFooter });
    }
  }
  // logDebug({ buttonPreList });

  return buttonPreList.map((it) => findButtonInTreeNode(it));
}
