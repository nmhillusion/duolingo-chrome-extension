import { getSessionPlayerFooter } from "./getSessionPlayerFooter";

export function getCorrectAnswerEl() {
  const sessionPlayerFooterEl_ = getSessionPlayerFooter();
  if (sessionPlayerFooterEl_) {
    return document.querySelector(`[data-test="blame blame-correct"]`);
  }

  return null;
}

export function getIncorrectAnswerEl() {
  const sessionPlayerFooterEl_ = getSessionPlayerFooter();
  if (sessionPlayerFooterEl_) {
    return document.querySelector(`[data-test="blame blame-incorrect"]`);
  }

  return null;
}