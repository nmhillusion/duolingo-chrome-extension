export function getProgressBarElement() {
  return document.querySelector(`[role="progressbar"]`) as HTMLDivElement;
}

export function getProgressBarThumbElement(
  existedProgressBarEl?: HTMLDivElement
) {
  return (existedProgressBarEl || getProgressBarElement())?.querySelector(
    `*:first-child *:nth-child(2)`
  ) as HTMLDivElement | null;
}
