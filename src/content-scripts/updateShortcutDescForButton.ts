import { findButtonInTreeNode } from "../module/findButtonInTreeNode";
import { getSelectableButton } from "./utility/getSelectableButton";
import { getSkipButton } from "./utility/getSkipButton";

export function updateShortcutDescForButton() {
  {
    const oldShortcutEls = document.querySelectorAll(
      ".n2-shortcut-desc.n2-answer-text"
    );
    for (const el_ of oldShortcutEls) {
      el_?.remove();
    }
  }

  const elements_ = getSelectableButton();

  if (0 < elements_.length) {
    let elNum = 0;
    for (const el_ of elements_) {
      const btnEl = findButtonInTreeNode(el_);
      if (btnEl && !btnEl.querySelector(".n2-shortcut-desc")) {
        elNum += 1;

        const shortcutEl = document.createElement("span");
        shortcutEl.classList.add("n2-shortcut-desc");
        shortcutEl.classList.add("n2-answer-text");
        shortcutEl.textContent = String(elNum % 10);

        btnEl.appendChild(shortcutEl);
      }
    }
  }

  const skipBtn = getSkipButton();
  if (skipBtn && !skipBtn.querySelector(".n2-shortcut-desc")) {
    const shortcutEl = document.createElement("span");
    shortcutEl.classList.add("n2-shortcut-desc");
    shortcutEl.classList.add("n2-answer-text");
    shortcutEl.textContent = " (Esc)";

    skipBtn.appendChild(shortcutEl);
  }
}
