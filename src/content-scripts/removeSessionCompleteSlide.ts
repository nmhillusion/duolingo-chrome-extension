import { getProgressBarElement } from "./utility/getProgressBarElement";
import { StateType } from "./utility/state";

export function removeSessionCompleteSlide(STATE: StateType) {
  if (STATE.CONFIG.running) {
    {
      const completeSessionEls = Array.from(
        document.querySelectorAll(
          [
            `[data-test="session-complete-slide"]`,
            `[data-test="daily-quest-progress-slide"]`,
          ].join(",")
        )
      );

      if (!getProgressBarElement()) {
        completeSessionEls.push(document.querySelector(`.wPvQK._18W4a.xtPuL`)); // element of popup to show result at the end of test
      }

      // logInfo({ completeSessionEls });

      if (0 < completeSessionEls.length) {
        for (const completeEl_ of completeSessionEls) {
          completeEl_?.parentElement?.parentElement?.remove();
        }
      }
    }

    {
      const rankBoardEls = document.querySelectorAll(".Lwvas._2C9ly");

      if (0 < rankBoardEls.length) {
        for (const rankEl of rankBoardEls) {
          rankEl?.parentElement?.remove();
        }
      }
    }
  }
}
