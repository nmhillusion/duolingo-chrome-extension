import { getSideBarContainer } from "./utility/getSideBarContainer";
import { isHomePage } from "./utility/isHomePage";
import { n2classComp } from "./utility/name.constant";

export function markN2KeepComponents() {
  if (isHomePage()) {
    const sidebarEl = getSideBarContainer();

    if (sidebarEl) {
      {
        const markEls = sidebarEl.querySelectorAll(
          `[data-test="courses-menu"]:not(.${n2classComp})`
        );
        markEls.forEach((el) => {
          el.parentElement.classList.add(n2classComp);
        });
      }

      {
        const markEls = sidebarEl.querySelectorAll(
          `[data-test="footer-links"]:not(.${n2classComp})`
        );
        markEls.forEach((el) => {
          el.classList.add(n2classComp);
        });
      }
    }
  }
}