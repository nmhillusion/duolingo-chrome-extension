/// <reference path="types/chrome.d.ts" />

import { CONFIG } from "./content-scripts/utility/config";
import { isHomePage } from "./content-scripts/utility/isHomePage";
import { markN2KeepComponents } from "./content-scripts/makeN2KeepComponents";
import { setupShortcuts } from "./content-scripts/setupShortcuts";
import { StateType } from "./content-scripts/utility/state";
import { SystemListener } from "./content-scripts/system.listener";
import { parseBoolean } from "./module/data-type.parser";
import { logDebug, logError, logInfo, logWarn } from "./module/log";
import { parseChromeRawValue } from "./module/parse-chrome-raw-value";
import { buildGoalOfDailyQuest } from "./content-scripts/buildGoalOfDailyQuest";
import { enableKeyboardMode } from "./content-scripts/enableKeyboardMode";
import { buildShortcutForCommonActions } from "./content-scripts/buildShortcutForCommonActions";
import { addStylesheetForDisableHint } from "./content-scripts/addStylesheetForDisableHint";
import { removeTournamentBoard } from "./content-scripts/removeTournamentBoard";
import { findAndClickNextButton } from "./content-scripts/utility/findAndClickNextButton";
import {
  monitorTestProgress,
  updateCorrectAnswerStatusPosition,
} from "./content-scripts/monitorTestProgress";
import { removePromotionRank } from "./content-scripts/removePromotionRank";
import { removeSessionCompleteSlide } from "./content-scripts/removeSessionCompleteSlide";
import { updateShortcutDescForButton } from "./content-scripts/updateShortcutDescForButton";
import { addN2StyleElement } from "./content-scripts/addN2StyleElement";
import { ElementObservation } from "./module/element-observation";
import { getRootElement } from "./content-scripts/utility/getRootElement";
import { isPracticePage } from "./content-scripts/utility/isPracticePage";
import { buildCurrentHeartsInPractice } from "./content-scripts/buildCurrentHeartsInPractice";

(function duolingoExt() {
  const STATE: StateType = {
    LISTENER: {
      EL_OBSERVABLE_LISTENER: {
        instance: new ElementObservation(),
        isRunning: false,
        startLoopFunc() {
          STATE.LISTENER.EL_OBSERVABLE_LISTENER.isRunning = true;
        },
        endLoopFunc() {
          STATE.LISTENER.EL_OBSERVABLE_LISTENER.isRunning = false;
        },
      },
    },
    TIMER: {
      intervalTicker: {
        instance: null,
        isRunning: false,
        latestTickTimestamp: null,
        startLoopFunc() {
          STATE.TIMER.intervalTicker.latestTickTimestamp = new Date();
          STATE.TIMER.intervalTicker.isRunning = true;
        },
        endLoopFunc() {
          STATE.TIMER.intervalTicker.isRunning = false;
        },
      },
    },
    currentUrl: "",
    CONFIG: {
      running: true,
      onlyShowExp: false,
      showHintOfWordBank: true,
      autoClickNext: true,
      showExtraStatusInTest: true,
      autoEnableToggleToKeyboard: true,
    },
    STORE: {
      monitor: {
        correctAnswers: 0,
        incorrectAnswers: 0,
        currentHeartsInPractice: -1,
      },
      cooling: {
        coolingMonitorNextButton: null,
        coolingClickNextButton: null,
      },
    },
  };

  const SYSTEM_LISTENER = new SystemListener(STATE);

  chrome.storage.sync.get(Object.keys(STATE.CONFIG), __main__);

  chrome.storage.onChanged.addListener((changes, namespace) => {
    const newStateConfig = Object.keys(changes).reduce((prev, currKey) => {
      prev[currKey] = changes[currKey].newValue;
      return prev;
    }, {} as { [key: string]: string | boolean });

    logDebug({ changes, newStateConfig, stateConfig: STATE.CONFIG });

    parseChromeRawValue(STATE.CONFIG, newStateConfig, parseBoolean);
    __main__(STATE.CONFIG);
  });

  function __main__(rawSettings: { [key: string]: boolean | string }) {
    parseChromeRawValue<boolean>(STATE.CONFIG, rawSettings, parseBoolean);

    logInfo(
      "___ main duolingo ext process ___ > running: ",
      STATE.CONFIG,
      rawSettings
    );

    markN2KeepComponents();

    STATE.LISTENER.EL_OBSERVABLE_LISTENER.instance.registerListener({
      isAbleToStart() {
        return !STATE.LISTENER.EL_OBSERVABLE_LISTENER.isRunning;
      },
      func: (mutation) => {
        try {
          STATE.LISTENER.EL_OBSERVABLE_LISTENER.startLoopFunc();
          // console.log("mutation elements event: ", mutation);
          const targetEl = mutation.target as HTMLElement;

          if ("progressbar" === targetEl?.getAttribute("role")) {
            if (!isHomePage() && STATE.CONFIG.showExtraStatusInTest) {
              monitorTestProgress(STATE);
            }

            if (isPracticePage() && STATE.CONFIG.showExtraStatusInTest) {
              buildCurrentHeartsInPractice(STATE);
            }
          }

          SYSTEM_LISTENER.detectChangePageUrl();
          SYSTEM_LISTENER.detectRePractice();

          if (mutation.addedNodes && 0 < mutation.addedNodes.length) {
            markN2KeepComponents();
            removeTournamentBoard(STATE);
            removePromotionRank(STATE);
            removeSessionCompleteSlide(STATE);
          }
        } catch (err) {
          logError(err);
        } finally {
          STATE.LISTENER.EL_OBSERVABLE_LISTENER.endLoopFunc();
        }
      },
    });
    STATE.LISTENER.EL_OBSERVABLE_LISTENER.instance.start(getRootElement());

    SYSTEM_LISTENER.registerForListeners();
    setupShortcuts(STATE);
    // updateBrowserSettings();

    removeTournamentBoard(STATE);

    if (STATE.TIMER.intervalTicker.instance) {
      clearInterval(STATE.TIMER.intervalTicker.instance);
    }

    STATE.TIMER.intervalTicker.instance = setInterval(
      inLoopAction,
      CONFIG.INTERVAL_IN_MILLIS
    );

    addN2StyleElement(STATE);
    addStylesheetForDisableHint(STATE);
  }

  function inLoopAction() {
    if (STATE.TIMER.intervalTicker.isRunning) {
      logWarn("Not running loop function due to not finished the last one");
      return;
    }

    try {
      STATE.TIMER.intervalTicker.startLoopFunc();

      markN2KeepComponents();
      removeTournamentBoard(STATE);
      removePromotionRank(STATE);
      removeSessionCompleteSlide(STATE);

      if (!isHomePage() && STATE.CONFIG.showExtraStatusInTest) {
        updateCorrectAnswerStatusPosition();
      }

      if (STATE.CONFIG.autoClickNext) {
        findAndClickNextButton(STATE);
      }
      updateShortcutDescForButton();
      SYSTEM_LISTENER.detectChangePageUrl();
      buildShortcutForCommonActions();

      if (STATE.CONFIG.autoEnableToggleToKeyboard) {
        enableKeyboardMode();
      }

      if (STATE.CONFIG.onlyShowExp) {
        buildGoalOfDailyQuest();
      }

      if (isPracticePage() && STATE.CONFIG.showExtraStatusInTest) {
        buildCurrentHeartsInPractice(STATE);
      }
    } catch (err) {
      logError(err);
    } finally {
      STATE.TIMER.intervalTicker.endLoopFunc();
    }
  }
})();
