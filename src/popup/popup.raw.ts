/// <reference path="../types/chrome.d.ts" />

import { parseBoolean } from "../module/data-type.parser";
import { logInfo } from "../module/log";
import { parseChromeRawValue } from "../module/parse-chrome-raw-value";

console.log(":: popup ::");

enum SettingType {
  BUTTON,
}

const STATE: {
  TEXT: {
    [key: string]: {
      settingType: SettingType;
      onStatusText: string;
      offStatusText: string;
    };
  };
  CONFIG: { [key: string]: boolean | string };
} = {
  TEXT: {
    running: {
      settingType: SettingType.BUTTON,
      onStatusText: "Is Running",
      offStatusText: "Is Stopped",
    },
    onlyShowExp: {
      settingType: SettingType.BUTTON,
      onStatusText: "Home Page Goal: Is Only show exp",
      offStatusText: "Home Page Goal: Default by Duolingo",
    },
    showHintOfWordBank: {
      settingType: SettingType.BUTTON,
      onStatusText: "Enabled hint of word bank",
      offStatusText: "Disabled hint of word bank",
    },
    autoClickNext: {
      settingType: SettingType.BUTTON,
      onStatusText: "Enabled: Auto click Next Buttons",
      offStatusText: "Disabled: Auto click Next Buttons",
    },
    showExtraStatusInTest: {
      settingType: SettingType.BUTTON,
      onStatusText: "Enabled: Show Extra Status in progress bar in Exam mode",
      offStatusText:
        "Disabled: NOT Show Extra Status in progress bar in Exam mode",
    },
    autoEnableToggleToKeyboard: {
      settingType: SettingType.BUTTON,
      onStatusText: 'Enabled: Auto enable "Toggle Keyboard Mode" in Duolingo',
      offStatusText: 'Disabled: Default "Toggle Keyboard Mode" of Duolingo',
    },
  },
  CONFIG: {
    running: false,
    onlyShowExp: false,
    showHintOfWordBank: false,
    autoClickNext: false,
    showExtraStatusInTest: false,
    autoEnableToggleToKeyboard: false,
  },
};

chrome.storage.sync.get(Object.keys(STATE.CONFIG), main);

///////// NOT NECESSARILY TO CHANGE BELOW IF ONLY ADD NEW OPTION TO CONFIG ////////////////////////////////////////////////////////////////

function main(rawSettings: { [key: string]: boolean | string }) {
  console.log(rawSettings);
  parseChromeRawValue(STATE.CONFIG, rawSettings, parseBoolean);
  toggleShowInformToReload(false);

  for (const settingName of Object.keys(STATE.CONFIG)) {
    if (SettingType.BUTTON == STATE.TEXT[settingName].settingType) {
      configForButtonSettingItem(settingName);
    }
  }
}

function configForButtonSettingItem(settingName: string) {
  logInfo("config setting for ", settingName);
  if ("boolean" !== typeof STATE.CONFIG[settingName]) {
    throw Error(
      "Data-type of this setting is not valid for configForButtonSettingItem"
    );
  }

  const btnEl = document.querySelector(
    "#btn-" + settingName
  ) as HTMLButtonElement;

  if (!btnEl) {
    console.error("Could not find button for setting name: ", settingName);
  }

  fillSwitchText(STATE.CONFIG[settingName], btnEl, STATE.TEXT[settingName]);

  btnEl.addEventListener("click", (e) => {
    STATE.CONFIG[settingName] = !STATE.CONFIG[settingName];
    saveToChromeExtensionStorage(
      settingName,
      STATE.CONFIG[settingName],
      console.error
    );
    fillSwitchText(STATE.CONFIG[settingName], btnEl, STATE.TEXT[settingName]);

    toggleShowInformToReload(true);
  });
}

function fillSwitchText(
  isStateOn: boolean | string,
  btn: HTMLButtonElement,
  stateText: {
    onStatusText: string;
    offStatusText: string;
  }
) {
  if ("boolean" != typeof isStateOn) {
    throw Error("Data-type of isStateOn is not a boolean: " + isStateOn);
  }

  const label_ = document.querySelector(
    `label[for="${btn.id}"]`
  ) as HTMLLabelElement | null;
  if (isStateOn) {
    if (label_) {
      label_.innerText = stateText.onStatusText;
    }
    btn.classList.add("on");
    btn.classList.remove("off");
  } else {
    if (label_) {
      label_.innerText = stateText.offStatusText;
    }
    btn.classList.add("off");
    btn.classList.remove("on");
  }
}

function saveToChromeExtensionStorage(
  key: string,
  value: boolean | string,
  onError: (error: any) => void
) {
  console.log("do save to chrome storage: ", { key, value });

  chrome.storage.sync.set({ [key]: String(value) }, onError);
}

function toggleShowInformToReload(show: boolean) {
  const showInformReloadPanel = document.querySelector(
    ".inform-to-reload"
  ) as HTMLParagraphElement;

  if (showInformReloadPanel) {
    if (show) {
      showInformReloadPanel.style.display = "block";
    } else {
      showInformReloadPanel.style.display = "none";
    }
  }
}
