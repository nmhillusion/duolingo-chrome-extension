/// <reference path="../types/window.d.ts" />

(function () {
  const mainForm = window.mainForm;
  if (!mainForm) {
    throw new Error("Missing Main Form");
  }
})();
