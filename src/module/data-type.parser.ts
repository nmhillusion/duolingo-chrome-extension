export function parseBoolean(value: any): boolean {
  if (1 === value) {
    return true;
  }

  if ("true" === String(value).toLowerCase()) {
    return true;
  }

  return false;
}
