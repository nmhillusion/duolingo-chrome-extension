const extensionLogName = "[n2duolingo]";

interface LogLevel {
  levelName: string;
  levelStyle: string;
}

const LEVEL = {
  DEBUG: {
    levelName: "DEBUG",
    levelStyle: "color: gray",
  },
  INFO: {
    levelName: "INFO",
    levelStyle: "color: #359bed",
  },
  WARN: {
    levelName: "WARN",
    levelStyle: "color: goldenrod",
  },
  ERROR: {
    levelName: "ERROR",
    levelStyle: "color: red",
  },
};

export function logError(...args: any[]) {
  doLog(LEVEL.ERROR, ...args);
}

export function logWarn(...args: any[]) {
  doLog(LEVEL.WARN, ...args);
}

export function logInfo(...args: any[]) {
  doLog(LEVEL.INFO, ...args);
}

export function logDebug(...args: any[]) {
  doLog(LEVEL.DEBUG, ...args);
}

function doLog(level_: LogLevel, ...args: any[]) {
  console.log(
    `%c${extensionLogName} %c[${level_.levelName}]`,
    "color: rgb(147, 211, 51)",
    level_.levelStyle,
    ...args
  );
}
