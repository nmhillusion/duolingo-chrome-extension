export function repeatActionWithDebounce(
  continueFunc_: () => boolean,
  repeatTimes: number,
  debounceTime: number,
  currentCount: number = 0
) {
  if (!continueFunc_()) {
    return;
  }

  if (currentCount < repeatTimes - 1) {
    const looper_ = setTimeout(() => {
      clearTimeout(looper_);
      repeatActionWithDebounce(
        continueFunc_,
        repeatTimes,
        debounceTime,
        currentCount + 1
      );
    }, debounceTime);
  }
}