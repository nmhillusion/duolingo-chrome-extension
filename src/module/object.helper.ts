export function checkExistsElementInTreeObject(obj: any, ...keyPath: string[]) {
  let currentObj = obj;

  for (const key of keyPath) {
    if (key in currentObj) {
      currentObj = currentObj[key];
    } else {
      return false;
    }
  }

  return true;
}
