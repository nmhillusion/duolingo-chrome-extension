export function findButtonInTreeNode(
  el_: Element,
  selectorToExclude?: string
): HTMLButtonElement | null {
  let el = el_;
  if (el instanceof HTMLButtonElement) {
    return el;
  }

  while (el && el.parentElement) {
    if (el.parentElement instanceof HTMLButtonElement) {
      return el.parentElement;
    } else {
      el = el.parentElement;
    }
  }

  if (selectorToExclude && el?.querySelector(selectorToExclude)) {
    return null;
  }

  return el_ as HTMLButtonElement;
}