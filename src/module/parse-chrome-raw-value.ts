export function parseChromeRawValue<T>(
  destination: { [key: string]: T },
  source: { [key: string]: boolean | string },
  converter: (input: unknown) => T
) {
  const keyList = Object.keys(destination);
  const sourceKeyList = Object.keys(source);

  for (const key_ of keyList) {
    if (sourceKeyList.includes(key_)) {
      destination[key_] = converter(source[key_]);
    }
  }
}
