import { prefillTextWithChar } from "./string.helper";

export function formatTimeInSeconds(seconds: number): string {
  const hours = Math.floor(seconds / 3600);
  const minutes = Math.floor((seconds - hours * 3600) / 60);
  const seconds_ = seconds - hours * 3600 - minutes * 60;

  return `${prefillTextWithChar(String(hours), 2, "0")}:${prefillTextWithChar(
    String(minutes),
    2,
    "0"
  )}:${prefillTextWithChar(String(seconds_), 2, "0")}`;
}
