import { logError, logWarn } from "./log";

export class ElementObservation {
  #listeners: {
    func: (mutationRecord: MutationRecord) => void;
    isAbleToStart: () => boolean;
  }[] = [];
  #observer: MutationObserver;
  #currentStateVersion: number = 0;

  public registerListener(listener: {
    func: (mutationRecord: MutationRecord) => void;
    isAbleToStart: () => boolean;
  }) {
    this.#listeners.push(listener);
  }

  public start(element: HTMLElement) {
    if (!element) {
      logError("Element to observable is null: ", element);
      return;
    }

    if (this.#observer) {
      this.#observer.disconnect();
    }

    this.#observer = new MutationObserver((mutations) => {
      this.#currentStateVersion += 1;

      for (const mutation of mutations) {
        this.triggerForListener(mutation);
      }
    });

    this.#observer.observe(element, {
      childList: true,
      subtree: true,
      attributes: true,
    });
  }

  private triggerForListener(mutation: MutationRecord) {
    for (const listener_ of this.#listeners) {
      const versionWhenTrigger = this.#currentStateVersion;
      while (
        !listener_.isAbleToStart() &&
        versionWhenTrigger === this.#currentStateVersion
      );

      if (versionWhenTrigger === this.#currentStateVersion) {
        listener_.func(mutation);
      } else {
        logWarn(
          "Ignore running of this time due to different version of state"
        );
      }
    }
  }
}
