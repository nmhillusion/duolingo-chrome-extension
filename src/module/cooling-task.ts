export interface CoolingState {
  startTime: number;
  isCooling: () => boolean;
  coolingTimeInMillis: number;
}

export function coolingTask(coolingTimeInMillis: number): CoolingState {
  return {
    startTime: new Date().getTime(),
    isCooling() {
      return coolingTimeInMillis > new Date().getTime() - this.startTime;
    },
    coolingTimeInMillis,
  };
}
