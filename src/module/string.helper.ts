export function prefillTextWithChar(
  text: string,
  length: number,
  string2fill: string = " "
): string {
  let txt = text;

  while (txt.length < length) {
    txt = string2fill + txt;
  }

  return txt;
}
