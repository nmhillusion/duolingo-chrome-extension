/// <reference path="./types/chrome.d.ts" />

const DEFAULT_CONFIG_ = {
  running: true,
  onlyShowExp: false,
  showHintOfWordBank: true,
  autoClickNext: true,
  showExtraStatusInTest: true,
  autoEnableToggleToKeyboard: true,
};

///////// NOT NECESSARILY TO CHANGE BELOW IF ONLY ADD NEW OPTION TO CONFIG ////////////////////////////////////////////////////////////////

function setDefaultForExtensionConfig(
  chromeStorageData: { [key: string]: any },
  configName: string,
  configValue: any
) {
  if (!(configName in chromeStorageData)) {
    chrome.storage.sync.set({ [configName]: configValue }, function () {
      console.log(`Set default [${configName}] successfully: `, configValue);
    });
  }
}

chrome.runtime.onInstalled.addListener(function () {
  const configKeyList = Object.keys(DEFAULT_CONFIG_);

  chrome.storage.sync.get(
    configKeyList,
    function (chromeStorageData: { [key: string]: any }) {
      for (const configKey_ of configKeyList) {
        setDefaultForExtensionConfig(
          chromeStorageData,
          configKey_,
          DEFAULT_CONFIG_[configKey_]
        );
      }
    }
  );

  chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
    chrome.declarativeContent.onPageChanged.addRules([
      {
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { hostContains: "duolingo.com" },
          }),
        ],
        actions: [new chrome.declarativeContent.ShowPageAction()],
      },
    ]);
  });
});
