import * as fs from "fs";
import { BullEngine } from "@nmhillusion/n2ngin-bull-engine";

import { parser } from "@nmhillusion/n2mix";
import path from "path";

const processArgv = parser.cliParamsParser(process.argv);

const isWatching = "watch" === processArgv.get("mode");

console.log({ processArgv, mode: processArgv.get("mode"), isWatching });

// exit(1);

const tsConfig = JSON.parse(fs.readFileSync("./tsconfig.json").toString());

const outDir = path.join(process.cwd(), "dist");
const rootDir = path.join(process.cwd(), "src");

console.log({ outDir, rootDir });

new BullEngine()
  .config({
    outDir,
    rootDir,
    copyResource: {
      enabled: true,
      config: {
        extsToCopy: [".jpg", ".jpeg", ".png", ".gif", ".ico", ".woff", ".ttf"],
      },
    },
    rewriteJavascript: {
      enabled: false,
      config: {
        compress: true,
        rewriteImport: true,
      },
    },
    pug: {
      enabled: true,
      config: {
        pretty: true,
      },
    },
    markdown: {
      enabled: false,
    },
    scss: {
      enabled: true,
      config: {
        style: "compressed",
      },
    },
    typescript: {
      enabled: true,
      overwriteAllConfig: false,
      config: tsConfig["compilerOptions"],
    },
    watch: {
      enabled: isWatching,
    },
  })
  .render();
