const path = require("path");

module.exports = {
  entry: {
    content: path.resolve(__dirname, "dist", "content.raw.js"),
    background: path.resolve(__dirname, "dist", "background.raw.js"),
    "popup/popup": path.resolve(__dirname, "dist", "popup", "popup.raw.js"),
    "option/options": path.resolve(
      __dirname,
      "dist",
      "option",
      "options.raw.js"
    ),
  },
  output: {
    filename: "[name].packed.js",
    path: path.resolve(__dirname, "dist"),
  },
};
